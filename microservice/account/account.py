from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, Response
from flask_sqlalchemy import SQLAlchemy
import os,sqlite3
import account.db
from app import app

cookie = session()

app.config['SQLALCHEMY_DATABASE_URI'] = 'account/account.db'
dataB = SQLAlchemy(app)

app.route("/login/<name>", methods=['GET'])
def login(name, password):
       return loginCheck(dataB, name)


def loginCheck(dataB, name):
    cursor = dataB.cursor()
    return cursor.execute("SELECT * FROM users WHERE name = ? AND password = ?",
                          (name)).fetchall()

@app.route("/logout")#incomplete
def logout():
    return Response('<p>Logged out</p>')

if __name__ == '__main__':
    app.run(port=5001, debug=True)