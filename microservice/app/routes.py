from flask import render_template
from app import app

@app.route('/')
@app.route('/index')
def index():
    item = {'href': '/qantas', 'caption': 'Qantas'}

    return render_template('index.html', navigation=item)

@app.route('/qantas')
def qantas():
    user = {'username': 'Qantas'}
    posts = [
        {
            'destination': {'city': 'Sydney'},
            'body': 'Not the Capital of Australia'
        },
        {
            'destination': {'city': 'Auckland'},
            'body': 'Not the Capital of Australia'
        }
    ]
    return render_template('qantas.html', title='Home', user=user, posts=posts)


@app.route('/united')
def united():
    user = {'username': 'united'}
    posts = [
        {
            'destination': {'city': 'Sydney'},
            'body': 'Not the Capital of Australia'
        },
        {
            'destination': {'city': 'Auckland'},
            'body': 'Not the Capital of Australia'
        }
    ]
    return render_template('united.html', title='united', user=user, posts=posts)

@app.route('/login', methods=['POST'])
def login():
    if request.form['password'] == 'password' and request.form['username'] == 'admin':
        return redirect('/', 302)


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404